package main

import (
	"fmt"

	"gitee.com/os4top_admin/restful-api-demo-g7/cmd"
)

func main() {
	if err := cmd.RootCmd.Execute(); err != nil {
		fmt.Printf("Error executing")
		fmt.Println(err)
	}
}

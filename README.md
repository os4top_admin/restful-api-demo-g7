# restful-api-demo-g7

#### 介绍
{**以下是 Gitee 平台说明，您可以替换此简介**
Gitee 是 OSCHINA 推出的基于 Git 的代码托管平台（同时支持 SVN）。专为开发者提供稳定、高效、安全的云端软件开发协作平台
无论是个人、团队、或是企业，都能够用 Gitee 实现代码托管、项目管理、协作开发。企业项目请看 [https://gitee.com/enterprises](https://gitee.com/enterprises)}

#### 软件架构
- Demo后端
功能: CMDB主机信息录入和查询

- 业务描述  
我们开发的是一个前后端分离的web service demo

demo只有基础功能就是对主机(Host)这种资源提供增删改查的基础操作(CRUD),我们基于Restful来架设我们的API server


#### API设计  
- GET: /hosts/ 查看主机列表
- GET: /hosts/:id/ 查询主机详情
- POST: /hosts/ 新增主机
- PUT: /hosts/:id 修改主机(全量)
- PATCH: /hosts/:id 修改主机(部分)
- DELETE: /hosts/:id 删除主机


#### 使用说明

```
cmd: CLI 
conf: 程序配置对象
protocol 程序监听的协议
version 程序自身的版本信息
apps: 业务
    host: 主机的增删改查的业务模块(host.go: 定义业务模型,struct和领域接口,关于这个业务对象处理的接口)
        model 业务需要的数据模型
        interface 业务接口(领域方向)
        impl 业务具体实现
    oss
    lb
main 程序入口文件
    
```
1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技


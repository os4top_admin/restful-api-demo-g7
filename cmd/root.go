package cmd

import (
	"fmt"

	"gitee.com/os4top_admin/restful-api-demo-g7/version"
	"github.com/spf13/cobra"
)

var vers bool
var RootCmd = &cobra.Command{
	Use:   "demo ",
	Long:  "demo 后端API",
	Short: "demo 后端API",
	RunE: func(cmd *cobra.Command, args []string) error {
		if vers {
			fmt.Println(version.FullVersion())
			return nil
		}
		return nil
	},
}

func init() {
	RootCmd.PersistentFlags().BoolVarP(&vers, "version", "v", false, "print demo-api version")
}

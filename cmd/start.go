package cmd

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"gitee.com/os4top_admin/restful-api-demo-g7/apps"
	_ "gitee.com/os4top_admin/restful-api-demo-g7/apps/all"
	"gitee.com/os4top_admin/restful-api-demo-g7/conf"
	"gitee.com/os4top_admin/restful-api-demo-g7/protocol"
	"github.com/infraboard/mcube/logger"
	"github.com/infraboard/mcube/logger/zap"
	"github.com/spf13/cobra"
)

var (
	confType string
	confFile string
	confEtcd string
)

// 程序的启动时 组装都在这里进行
// 1.
// StartCmd
var StartCmd = &cobra.Command{
	Use:   "start ",
	Long:  "启动 demo 后端API",
	Short: "启动 demo 后端API",
	RunE: func(cmd *cobra.Command, args []string) error {
		// 加载程序配置
		err := conf.LoadConfigFromToml(confFile)
		if err != nil {
			fmt.Println("loadConfigFromToml err")
			return err
		}

		// 初始化全局日志Logger
		if err := loadGlobalLogger(); err != nil {
			fmt.Println("loadGlobalLogger err")
			return err
		}

		fmt.Println("loadGlobalLogger success")
		// 加载我们Host Service的实体类
		// service := impl.NewHostServiceImpl()

		// 注册HostService的实例到IOC
		// 采用: _ "gitee.com/os4top_admin/restful-api-demo-g7/apps/host/impl" 完成注册
		// apps.HostService = impl.NewHostServiceImpl()

		// 如何执行HostService的config方法
		// 因为apps.HostService是一个host.Service接口,并没有包含实例初始化(Config)
		apps.InitImpl()
		fmt.Println("InitImpl...")

		// 通过Host API Handler提供 HTTP RestFul接口
		// api := http.NewHostHTTPHandler()
		// 从IOC中获取依赖,解除了相互依赖关系
		// api.Config()

		// 提供一个Gin Router
		// g := gin.Default()
		// // 注册IOC的所有http handler
		// apps.InitGin(g)

		// return g.Run(conf.C().App.HttpAddr())
		svc := newManager()
		fmt.Println("newManager...")

		ch := make(chan os.Signal, 1)
		defer close(ch)
		signal.Notify(ch, syscall.SIGTERM, syscall.SIGQUIT, syscall.SIGHUP, syscall.SIGINT)
		go svc.WaitStop(ch)

		fmt.Println("WaitStop...")
		fmt.Println("发生错误: ")
		return svc.Start()

	},
}

func newManager() *manager {
	return &manager{
		http: protocol.NewHttpService(),
		l:    zap.L().Named("CLI"),
	}
}

// 管理所有需要启动的服务
// 1.HTTP服务的启动
// 2.
type manager struct {
	http *protocol.HttpService
	l    logger.Logger
}

func (m *manager) Start() error {
	fmt.Println("start...")
	return m.http.Start()
}

// 处理来自外部的中断信号,比如Terminal
func (m *manager) WaitStop(ch <-chan os.Signal) {
	for v := range ch {
		switch v {
		default:
			m.l.Info("received signal: %s", v)
			m.http.Stop()
		}
	}
}

// log 为全局变量, 只需要load 即可全局可用户, 依赖全局配置先初始化
func loadGlobalLogger() error {
	var (
		logInitMsg string
		level      zap.Level
	)
	lc := conf.C().Log
	lv, err := zap.NewLevel(lc.Level)
	if err != nil {
		logInitMsg = fmt.Sprintf("%s, use default level INFO", err)
		level = zap.InfoLevel
	} else {
		level = lv
		logInitMsg = fmt.Sprintf("log level: %s", lv)
	}
	zapConfig := zap.DefaultConfig()
	zapConfig.Level = level
	zapConfig.Files.RotateOnStartup = false

	// 配置日志的输出方式
	switch lc.To {
	case conf.ToStdout:
		// 把日志打印到标准输出
		zapConfig.ToStderr = true
		// 并没有把日志输出到文件
		zapConfig.ToFiles = false
	case conf.ToFile:
		zapConfig.Files.Name = "api.log"
		zapConfig.Files.Path = lc.PathDir
	}

	// 配置日志的输出格式
	switch lc.Format {
	case conf.JSONFormat:
		zapConfig.JSON = true
	}

	// 把配置应用到全局Logger
	if err := zap.Configure(zapConfig); err != nil {
		return err
	}
	zap.L().Named("INIT").Info(logInitMsg)
	return nil
}

func init() {
	StartCmd.PersistentFlags().StringVarP(&confFile, "config", "f", "etc/demo.toml", "demo api 配置文件路径")
	RootCmd.AddCommand(StartCmd)
}

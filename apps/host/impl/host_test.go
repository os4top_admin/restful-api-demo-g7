package impl_test

import (
	"context"
	"fmt"
	"testing"

	"gitee.com/os4top_admin/restful-api-demo-g7/apps/host"
	"gitee.com/os4top_admin/restful-api-demo-g7/apps/host/impl"
	"gitee.com/os4top_admin/restful-api-demo-g7/conf"
	"github.com/infraboard/mcube/logger/zap"
	"github.com/stretchr/testify/assert"
)

var (
	// 定义对象是满足该接口的实例
	service host.Service
)

func TestCreate(t *testing.T) {
	should := assert.New(t)
	ins := host.NewHost()
	ins.Id = "ins-01"
	ins.Name = "test"
	ins.Region = "ch-hangzhou"
	ins.Type = "sm1"
	ins.CPU = 1
	ins.Memory = 2048

	ins, err := service.CreateHost(context.Background(), ins)
	if should.NoError(err) {
		fmt.Println(ins)
	}

}

func init() {
	err := conf.LoadConfigFromEnv()
	if err != nil {
		fmt.Println("panic(err) init")
		panic(err)
	}
	// 需要初始化全局Logger,
	// 为什么不涉及为默认打印,因为性能
	zap.DevelopmentSetup()

	// host service 的具体实现
	service = impl.NewHostServiceImpl()

}

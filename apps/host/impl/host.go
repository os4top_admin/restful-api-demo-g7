package impl

import (
	"context"

	"gitee.com/os4top_admin/restful-api-demo-g7/apps/host"
	"github.com/infraboard/mcube/logger"
)

// 业务处理层(Controller)
func (i *HostServiceImpl) CreateHost(ctx context.Context, ins *host.Host) (*host.Host, error) {

	// 直接打印日志
	i.l.Named("Create").Debug("create host")
	i.l.Debug("create host")
	// 带Format的日志打印,fmt.Sprintf()
	i.l.Debugf("create host %s", ins.Name)
	// 携带额外的meta数据,常用于Trance系统
	i.l.With(logger.NewAny("request-id", "req01")).Debug("create host with meta kv")

	// 校验数据合法性
	if err := ins.Validate(); err != nil {
		return nil, err
	}

	// 默认值填充
	ins.InjectDefault()

	// 有dao模块 负载数据库入库
	if err := i.Save(ctx, ins); err != nil {
		return nil, err
	}
	return nil, nil
}

func (i *HostServiceImpl) QueryHost(ctx context.Context, req *host.QureyHostRequest) (
	*host.HostSet, error) {

	return nil, nil
}

func (i *HostServiceImpl) DescribeHost(ctx context.Context, req *host.QureyHostRequest) (
	*host.Host, error) {

	return nil, nil
}

func (i *HostServiceImpl) UpdateHost(ctx context.Context, req *host.UpdateHostRequest) (
	*host.Host, error) {

	return nil, nil
}

func (i *HostServiceImpl) DeleteHost(ctx context.Context, req *host.DeleteHostRequest) (
	*host.Host, error) {

	return nil, nil
}

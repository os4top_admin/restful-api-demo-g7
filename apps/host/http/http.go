package http

import (
	"fmt"

	"gitee.com/os4top_admin/restful-api-demo-g7/apps"
	"gitee.com/os4top_admin/restful-api-demo-g7/apps/host"
	"github.com/gin-gonic/gin"
)

// 面向接口,正真Service的实现,在服务实例化的时候传递进来
// 也就是CLI的start
// func NewHostHTTPHandler() *Handler {
// 	return &Handler{}
// }

var handler = &Handler{}

// 通过写一个实体类,把内部的接口通过HTTP协议暴露出去
// 所以需要依赖内部接口的实现
// 该实体类,会实现Gin的Http Handler
type Handler struct {
	svc host.Service
}

func (h *Handler) Config() {
	if apps.HostService == nil {
		fmt.Println("panic err dependence host service required ")
		panic("dependence host service required")
	}
	// 从IOC里面获取HostService的实例对象
	h.svc = apps.GetImpl(host.AppName).(host.Service)
}

// 完成了 Http Handler的注册
func (h *Handler) Registry(r gin.IRouter) {
	fmt.Println("register...")
	r.POST("/hosts", h.CreateHost)
	fmt.Println("register...")

}

func (h *Handler) Name() string {
	return host.AppName
}

// 完成Http Handler注册
func init() {
	fmt.Println("start 完成Http Handler注册")
	apps.RegistryGin(handler)
	fmt.Println("start 完成Http Handler注册...")
}

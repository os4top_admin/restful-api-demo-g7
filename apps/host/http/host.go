package http

import (
	"fmt"

	"gitee.com/os4top_admin/restful-api-demo-g7/apps/host"
	"github.com/gin-gonic/gin"
	"github.com/infraboard/mcube/http/response"
)

// 用于暴露Host service接口

func (h *Handler) CreateHost(c *gin.Context) {
	fmt.Println("create host 进行接口调用")
	ins := host.NewHost()
	// 将HTTP协议里面 解析出来用户的请求参数
	// read c.Request.Body
	// json unmarshal

	// 用户传递过来的参数进行解析,实现一个json的unmarshal
	if err := c.Bind(ins); err != nil {
		response.Failed(c.Writer, err)
		return
	}

	// 进行接口调用

	ins, err := h.svc.CreateHost(c.Request.Context(), ins)
	if err != nil {
		response.Failed(c.Writer, err)
		return
	}
	response.Success(c.Writer, ins)
}
